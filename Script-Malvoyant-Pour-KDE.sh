#!/bin/bash
 
cat <<FIN > ~/.config/kdeglobals
[$Version]
update_info=filepicker.upd:filepicker-remove-old-previews-entry,fonts_global.upd:Fonts_Global,fonts_global_toolbar.upd:Fonts_Global_Toolbar
 
[ColorEffects:Disabled]
ChangeSelectionColor=
Color=56,56,56
ColorAmount=0.5
ColorEffect=2
ContrastAmount=0.5
ContrastEffect=1
Enable=
IntensityAmount=0.05
IntensityEffect=0
 
[ColorEffects:Inactive]
ChangeSelectionColor=true
Color=0,0,0
ColorAmount=0.5
ColorEffect=1
ContrastAmount=0.25
ContrastEffect=2
Enable=false
IntensityAmount=0
IntensityEffect=0
 
[Colors:Button]
BackgroundAlternate=12,12,12
BackgroundNormal=0,0,0
DecorationFocus=192,192,192
DecorationHover=255,255,255
ForegroundActive=192,255,255
ForegroundInactive=160,160,160
ForegroundLink=128,181,255
ForegroundNegative=255,128,172
ForegroundNeutral=255,212,128
ForegroundNormal=255,255,255
ForegroundPositive=128,255,128
ForegroundVisited=192,128,255
 
[Colors:Complementary]
BackgroundAlternate=0,16,29
BackgroundNormal=0,16,29
DecorationFocus=9,31,53
DecorationHover=9,31,53
ForegroundActive=246,116,0
ForegroundInactive=8,28,48
ForegroundLink=61,174,230
ForegroundNegative=237,21,21
ForegroundNeutral=201,206,59
ForegroundNormal=245,245,255
ForegroundPositive=17,209,22
ForegroundVisited=61,174,230
 
[Colors:Selection]
BackgroundAlternate=0,52,116
BackgroundNormal=0,49,110
DecorationFocus=192,192,192
DecorationHover=255,255,255
ForegroundActive=192,255,255
ForegroundInactive=96,148,207
ForegroundLink=128,181,255
ForegroundNegative=255,128,172
ForegroundNeutral=255,212,128
ForegroundNormal=255,255,255
ForegroundPositive=128,255,128
ForegroundVisited=192,128,255
 
[Colors:Tooltip]
BackgroundAlternate=12,12,12
BackgroundNormal=0,0,0
DecorationFocus=192,192,192
DecorationHover=255,255,255
ForegroundActive=192,255,255
ForegroundInactive=160,160,160
ForegroundLink=128,181,255
ForegroundNegative=255,128,172
ForegroundNeutral=255,212,128
ForegroundNormal=255,255,255
ForegroundPositive=128,255,128
ForegroundVisited=192,128,255
 
[Colors:View]
BackgroundAlternate=12,12,12
BackgroundNormal=0,0,0
DecorationFocus=192,192,192
DecorationHover=255,255,255
ForegroundActive=192,255,255
ForegroundInactive=160,160,160
ForegroundLink=128,181,255
ForegroundNegative=255,128,172
ForegroundNeutral=255,212,128
ForegroundNormal=255,255,255
ForegroundPositive=128,255,128
ForegroundVisited=192,128,255
 
[Colors:Window]
BackgroundAlternate=20,20,20
BackgroundNormal=16,16,16
DecorationFocus=192,192,192
DecorationHover=255,255,255
ForegroundActive=192,255,255
ForegroundInactive=160,160,160
ForegroundLink=128,181,255
ForegroundNegative=255,128,172
ForegroundNeutral=255,212,128
ForegroundNormal=255,255,255
ForegroundPositive=128,255,128
ForegroundVisited=192,128,255
 
[General]
BrowserApplication=opera.desktop
ColorScheme=Zion (Reversed)
Name=Starcraft
XftHintStyle=
XftSubPixel=
dbfile=/home/jacky/.mozilla/firefox/6yr3wpfx.default/places.sqlite
fixed=Hack,11,-1,5,50,0,0,0,0,0,Regular
font=Noto Sans,12,-1,5,50,0,0,0,0,0,Regular
menuFont=Noto Sans,12,-1,5,50,0,0,0,0,0,Regular
shadeSortColumn=true
smallestReadableFont=Noto Sans,12,-1,5,50,0,0,0,0,0,Regular
toolBarFont=Noto Sans,12,-1,5,50,0,0,0,0,0,Regular
widgetStyle=Breeze
 
[Icons]
Theme=breeze-dark
 
[KDE]
ColorScheme=Breeze
LookAndFeelPackage=org.kde.starcraft
contrast=2
widgetStyle=kvantum
 
[KFileDialog Settings]
Automatically select filename extension=true
Breadcrumb Navigation=true
Decoration position=0
LocationCombo Completionmode=5
PathCombo Completionmode=5
Show Bookmarks=false
Show Full Path=false
Show Inline Previews=true
Show Preview=false
Show Speedbar=true
Show hidden files=false
Sort by=Name
Sort directories first=true
Sort reversed=false
Speedbar Width=148
View Style=Simple
listViewIconSize=0
 
[KScreen]
ScaleFactor=1.5
ScreenScaleFactors=DP-1-1=1.5;HDMI-1-1=1.5;HDMI-1-2=1.5;DP-1-2=1.5;DVI-D-1=1.5;HDMI-3=1.5;DP-3=1.5;
 
[WM]
activeBackground=0,49,110
activeBlend=0,16,29
activeFont=Noto Sans,12,-1,5,50,0,0,0,0,0,Regular
activeForeground=255,255,255
inactiveBackground=64,64,64
inactiveBlend=0,16,29
inactiveForeground=128,128,128
FIN
 
cat <<FIN > ~/.config/kcmfonts
[General]
dontChangeAASettings=true
forceFontDPI=144
FIN
 
cat <<FIN > ~/.config/Trolltech.conf
[qt]
5.11\libraryPath=
GUIEffects=none
KDE\contrast=2
KWinPalette\activeBackground=#00316e
KWinPalette\activeBlend=#00101d
KWinPalette\activeForeground=#ffffff
KWinPalette\activeTitleBtnBg=#101010
KWinPalette\frame=#101010
KWinPalette\inactiveBackground=#404040
KWinPalette\inactiveBlend=#00101d
KWinPalette\inactiveForeground=#808080
KWinPalette\inactiveFrame=#101010
KWinPalette\inactiveTitleBtnBg=#101010
Palette\active=#ffffff, #000000, #868686, #6c6c6c, #5a5a5a, #424242, #ffffff, #ffffff, #ffffff, #000000, #101010, #6c6c6c, #00316e, #fffff$
Palette\disabled=#606060, #1c1c1c, #383838, #2f2f2f, #171717, #202020, #5c5c5c, #ffffff, #5c5c5c, #1c1c1c, #242424, #101010, #242424, #606$
Palette\inactive=#ffffff, #000000, #868686, #6c6c6c, #5a5a5a, #424242, #ffffff, #ffffff, #ffffff, #000000, #101010, #6c6c6c, #021f42, #fff$
font="Noto Sans,12,-1,5,50,0,0,0,0,0,Regular"
FIN
